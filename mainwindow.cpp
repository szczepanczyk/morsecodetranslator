#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionLoad_source, SIGNAL(triggered()), this, SLOT(actionLoad_source_triggered()));
    connect(ui->actionSave_target, SIGNAL(triggered()), this, SLOT(actionSave_target_triggered()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnTranslate_clicked()
{
    if(translator.isMorseCode(ui->textEditInput->toPlainText()))
    {
        ui->textEditOutput->setText(translator.morseCodeToText(ui->textEditInput->toPlainText()));
    }
    else ui->textEditOutput->setText(translator.textToMorseCode(ui->textEditInput->toPlainText()));
}

void MainWindow::actionLoad_source_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Load file",
                                                    QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).first(),
                                                    "Text files (*.txt)");
    if(fileName.isEmpty())
        return;

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        ui->textEditInput->setText(QString(file.readAll()));
        file.close();
    }
}

void MainWindow::actionSave_target_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save File",
                                                    QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).first(),
                                                    "Text files (*.txt)");
    if(fileName.isEmpty())
        return;

    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text ))
    {
        QTextStream out(&file);
        out << ui->textEditOutput->toPlainText();
        file.close();
    }
}
