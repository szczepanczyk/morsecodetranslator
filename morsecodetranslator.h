#ifndef MORSECODETRANSLATOR_H
#define MORSECODETRANSLATOR_H

#include <QObject>
#include <QMap>

class MorseCodeTranslator : public QObject
{
    Q_OBJECT
public:
    explicit MorseCodeTranslator(QObject *parent = 0);
    ~MorseCodeTranslator();
    QString textToMorseCode(const QString &text);
    QString morseCodeToText(const QString &code);
    bool isMorseCode(const QString &text);
private:
    QString charToMorseCode(const QChar& c);
    QChar morseCodeToChar(const QString &code);
    typedef QMap<QChar, QString> MorseCodeMap;
    static MorseCodeMap morseCodeMap;

};

#endif // MORSECODETRANSLATOR_H
