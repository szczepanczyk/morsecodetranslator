#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "morsecodetranslator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnTranslate_clicked();
    void actionLoad_source_triggered();
    void actionSave_target_triggered();

private:
    Ui::MainWindow *ui;
    MorseCodeTranslator translator;
};

#endif // MAINWINDOW_H
