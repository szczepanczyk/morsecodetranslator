#-------------------------------------------------
#
# Project created by QtCreator 2015-03-22T11:04:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MorseCodeTranslator
TEMPLATE = app
QMAKE_CXXFLAGS+= -std=c++11
QMAKE_LFLAGS +=  -std=c++11
DEFINES *= QT_USE_QSTRINGBUILDER

SOURCES += main.cpp\
        mainwindow.cpp \
    morsecodetranslator.cpp

HEADERS  += mainwindow.h \
    morsecodetranslator.h

FORMS    += mainwindow.ui
