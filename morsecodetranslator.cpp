#include "morsecodetranslator.h"
#include <QRegExp>
#include <QStringList>

MorseCodeTranslator::MorseCodeTranslator(QObject *parent) : QObject(parent) {}

MorseCodeTranslator::~MorseCodeTranslator() {}

QString MorseCodeTranslator::textToMorseCode(const QString &text)
{
    QString retVal = "";
    for (QString::const_iterator i = text.cbegin(); i != text.cend() ; i++) {
        QChar chr = *i;
        QChar nextChr = *(i+1);
        retVal += charToMorseCode(chr);
        if(chr != ' ' && nextChr != ' ') retVal += " ";
    }
    return retVal.trimmed();
}

QString MorseCodeTranslator::morseCodeToText(const QString &code)
{
    QString retVal = "";
    QStringList words = code.split("  ");
    //O(n^2) needs to be improved in future.
    for (QStringList::const_iterator wordIterator = words.constBegin(); wordIterator != words.constEnd(); ++wordIterator)
    {
        QString word = *wordIterator;
        QStringList letters = word.split(' ');
        for(QStringList::const_iterator letterIterator = letters.constBegin() ; letterIterator != letters.constEnd(); ++letterIterator)
        {
            retVal += morseCodeToChar(*letterIterator);
        }
        retVal += " ";
    }
    return retVal;
}

bool MorseCodeTranslator::isMorseCode(const QString &text)
{
    //check for one or more occurence of dot, dash, space or line break chars in text.
    QRegExp regexp("[.- \n?]+");
    return regexp.exactMatch(text);
}

QString MorseCodeTranslator::charToMorseCode(const QChar &c)
{
    MorseCodeMap::iterator iter = morseCodeMap.find(c.toUpper());
    if(iter != morseCodeMap.end())
        return iter.value();
    else return "";
}

QChar MorseCodeTranslator::morseCodeToChar(const QString &code)
{
    return morseCodeMap.key(code);
}

MorseCodeTranslator::MorseCodeMap MorseCodeTranslator::morseCodeMap = {
    {' ', "  "},{'\n', "\n"},{'A', ".-"},{'B', "-..."},{'C', "-.-."},
    {'D', "-.."},{'E', "."},{'F', "..-."},{'G', "--."},{'H', "...."},
    {'I', ".."},{'J', ".---"},{'K', "-.-"},{'L', ".-.."},{'M', "--"},
    {'N', "-."},{'O', "---"},{'P', ".--."},{'Q', "--.-"},{'R', ".-."},
    {'S', "..."},{'T', "-"},{'U', "..-"},{'V', "...-"},{'W', ".--"},
    {'X', "-..-"},{'Y', "-.--"},{'Z', "--.."},{'1', ".----"},{'2', "..---"},
    {'3', "...--"},{'4', "....-"},{'5', "....."},{'6', "-...."},
    {'7', "--..."},{'8', "---.."},{'9', "----."},{'0', "-----"},
    {'.', ".-.-.-"},{',', "--..--"},{'?', "..--.."},{'-', "-...-"},
    {'/', "-..-."},{'@', ".--.-."},{'+', ".-.-."},{'=', "-..."}
};

